package com.d4iot.oss.plugin.packager.jmb;

import java.io.File;

import org.apache.maven.plugin.testing.AbstractMojoTestCase;

public class JarManifestBuilderTest extends AbstractMojoTestCase {
    /**
     * @throws Exception
     *             if any
     */
    public void testSomething() throws Exception {
        final File pom =
                getTestFile("src/test/resources/project-to-test-1/pom.xml");
        assertNotNull(pom);
        assertTrue(pom.exists());

        final JarManifestBuilderMojo myMojo =
                (JarManifestBuilderMojo) lookupMojo("process", pom);
        assertNotNull(myMojo);
        // need to get an executed pom to test
        // myMojo.execute();

    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();

    }

}
