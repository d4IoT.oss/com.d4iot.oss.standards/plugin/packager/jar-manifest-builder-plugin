package com.d4iot.oss.plugin.packager.api;

import java.util.ArrayList;
import java.util.List;

public class DependencyManifest {

    public static class Info {
        private String groupId;
        private String atrifact;
        private String version;
        private String id;
        private String name;
        private String scope;
        private String location;

        private String sha1;

        public String getAtrifact() {
            return atrifact;
        }

        public String getGroupId() {
            return groupId;
        }

        public String getId() {
            return id;
        }

        public String getLocation() {
            return location;
        }

        public String getName() {
            return name;
        }

        public String getScope() {
            return scope;
        }

        public String getSha1() {
            return sha1;
        }

        public String getVersion() {
            return version;
        }

        public void setAtrifact(
                final String atrifact) {
            this.atrifact = atrifact;
        }

        public void setGroupId(
                final String groupId) {
            this.groupId = groupId;
        }

        public void setId(
                final String id) {
            this.id = id;
        }

        public void setLocation(
                final String location) {
            this.location = location;
        }

        public void setName(
                final String name) {
            this.name = name;
        }

        public void setScope(
                final String scope) {
            this.scope = scope;
        }

        public void setSha1(
                final String sha1) {
            this.sha1 = sha1;
        }

        public void setVersion(
                final String version) {
            this.version = version;
        }

    }

    private String groupId;
    private String atrifactId;
    private String version;
    private final List<Info> dependencies = new ArrayList<>();

    public String getAtrifactId() {
        return atrifactId;
    }

    public List<Info> getDependencies() {
        return dependencies;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getVersion() {
        return version;
    }

    public void setAtrifactId(
            final String atrifactId) {
        this.atrifactId = atrifactId;
    }

    public void setGroupId(
            final String groupId) {
        this.groupId = groupId;
    }

    public void setVersion(
            final String version) {
        this.version = version;
    }

}
