package com.d4iot.oss.plugin.packager.jmb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.versioning.OverConstrainedVersionException;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectHelper;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.shared.transfer.artifact.resolve.ArtifactResolver;

import com.d4iot.oss.plugin.packager.api.DependencyManifest;
import com.google.common.io.Files;
import com.jsoniter.output.JsonStream;

@Mojo(name = "process", defaultPhase = LifecyclePhase.PACKAGE,
        threadSafe = true,
        requiresDependencyResolution = ResolutionScope.RUNTIME)
public class JarManifestBuilderMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project}", readonly = true,
            required = true)
    private MavenProject project;
    @Parameter(defaultValue = "${settings.localRepository}",
            readonly = true, required = true)
    private String localRepoPath;
    @Component
    private MavenProjectHelper projectHelper;
    @Component
    protected ArtifactResolver artifactResolver;
    @Component
    private ProjectBuilder projectBuilder;
    @Parameter(defaultValue = "${session}", readonly = true,
            required = true)
    private MavenSession session;

    @Override
    public void execute()
            throws MojoExecutionException, MojoFailureException {
        if ("pom".equals(project.getPackaging())) {
            getLog().info("Updating dependency Manifest - Skipping for POM");
            return;
        }

        getLog().info("Updating dependency Manifest");
        final String repoPathPrefix =
                new File(localRepoPath).getAbsolutePath();
        final int repoPathLength = repoPathPrefix.length();
        final Map<String, DependencyManifest.Info> infoList =
                new HashMap<>();
        final DependencyManifest dm = new DependencyManifest();
        dm.setAtrifactId(project.getArtifactId());
        dm.setGroupId(project.getGroupId());
        dm.setVersion(project.getVersion());

        for (final Artifact artifact : project.getArtifacts()) {
            final DependencyManifest.Info info =
                    new DependencyManifest.Info();

            info.setId(getId(artifact.getGroupId(),
                             artifact.getArtifactId(),
                             artifact.getClassifier()));
            infoList.put(info.getId(), info);
            final String name = artifact.getGroupId() + "_"
                    + artifact.getFile().getName();
            info.setName(name);
            final String filePath = artifact.getFile().getAbsolutePath();
            if (filePath.startsWith(repoPathPrefix)) {
                info.setLocation(filePath.substring(repoPathLength)
                        .replace('\\', '/'));
            } else {
                info.setLocation("");
            }

            info.setAtrifact(artifact.getFile().getName());
            info.setGroupId(artifact.getGroupId());
            info.setScope(artifact.getScope());
            try {
                info.setVersion(artifact.getSelectedVersion().toString());
                info.setSha1(DigestUtils.sha1Hex(FileUtils
                        .readFileToByteArray(artifact.getFile())));
            } catch (final IOException
                    | OverConstrainedVersionException e) {
                getLog().warn("Failed to hash", e);
                info.setSha1("Failed");
            }

        }

        dm.getDependencies().addAll(infoList.values());

        final StringBuilder cpEntries = new StringBuilder();
        for (final DependencyManifest.Info inf : infoList.values()) {
            if ("test".equals(inf.getScope())) {
                continue;
            }
            cpEntries.append(" ").append(inf.getName());
        }

        updateJar(dm, cpEntries);

        getLog().info("Updating dependency Manifest - Completed");
    }

    private String getId(
            final String gId,
            final String aId,
            final String cl) {
        final String sep = ":";
        return (gId + sep + aId + sep + cl);
    }

    private void updateJar(
            final DependencyManifest dm,
            final StringBuilder cpEntries) throws MojoFailureException {

        try (final JarFile srcJar =
                new JarFile(project.getArtifact().getFile())) {
            Manifest manifest = srcJar.getManifest();
            if (manifest == null) {
                getLog().debug("Jar has no manifest - Creating one");
                manifest = new Manifest();
                manifest.getMainAttributes()
                        .put(Attributes.Name.MANIFEST_VERSION, "1.0");

            }
            final String dmLocDir =
                    "META-INF/com.d4iot.oss/manifest/dependency/";
            final String dmLoc = dmLocDir + "dependency-manifest.json";
            final File dmLocFile = new File(
                    project.getBuild().getOutputDirectory(), dmLoc);
            dmLocFile.getParentFile().mkdirs();
            manifest.getMainAttributes().put(Attributes.Name.CLASS_PATH,
                                             cpEntries.toString());

            final byte[] dmBytes = JsonStream.serialize(dm).getBytes();
            final File outDir =
                    new File(project.getBuild().getDirectory());
            final File tmpJar =
                    File.createTempFile("tempArtifactJar", ".tmp", outDir);
            try (final OutputStream fos = new FileOutputStream(tmpJar);
                    final JarOutputStream jos =
                            new JarOutputStream(fos, manifest)) {
                final Enumeration<JarEntry> jarEntries = srcJar.entries();
                while (jarEntries.hasMoreElements()) {
                    final JarEntry entry = jarEntries.nextElement();
                    if (entry.getName().equals(dmLoc)
                            || entry.getName().equals(dmLocDir)
                            || entry.getName()
                                    .equals("META-INF/MANIFEST.MF")) {
                        continue;
                    }
                    final InputStream entryInputStream =
                            srcJar.getInputStream(entry);
                    jos.putNextEntry(entry);
                    final byte[] buffer = new byte[4096];
                    int bytesRead = 0;
                    while ((bytesRead =
                            entryInputStream.read(buffer)) != -1) {
                        jos.write(buffer, 0, bytesRead);
                    }
                }
                jos.putNextEntry(new JarEntry(dmLocDir));
                final JarEntry entry = new JarEntry(dmLoc);
                jos.putNextEntry(entry);
                jos.write(dmBytes);
                Files.write(dmBytes, dmLocFile);
            }
            Files.move(tmpJar, project.getArtifact().getFile());
        } catch (final IOException exc) {
            throw new MojoFailureException("Failed to update jar", exc);

        }

    }
}
