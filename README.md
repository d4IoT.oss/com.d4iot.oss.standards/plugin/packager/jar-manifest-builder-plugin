# jar-manifest-builder-plugin

## Purpose
The plugin runs as part of the package phase. It creates an asset file (json format) that includes all the dependencies (including transitive) as well as update the lib attribute of the jar to point to the dependencies.

## usage

Add the following maven co-ordinates
```
    <plugin>
      <groupId>com.d4iot.oss.plugin.packager</groupId>
      <artifactId>jar-manifest-builder-plugin</artifactId>
      <version>1.0.0-3</version>
      <executions>
        <execution>
          <goals>
            <goal>process</goal>
          </goals>
        </execution>
      </executions>
    </plugin>
```

## Use Cases
### Audit and reporting
Optimize the process by which tooling will generate reports for audit and compliance within an organization.

### Reduce local repository storage requirements
Since the dependent jars are listed as part of the jar packaging, the recreation of identical runtime environment is possible. This storage requirements for keeping `shaded` or `uber` jars in repositories is significantly reduced.

## Sample Output

```
{
    "groupId": "com.d4iot.oss.plugin.packager",
    "atrifactId": "jar-manifest-builder-plugin",
    "version": "1.0.0-3",
    "dependencies": [
        {
            "name": "org.apache.maven.resolver_maven-resolver-spi-1.3.1.jar",
            "location": "/org/apache/maven/resolver/maven-resolver-spi/1.3.1/maven-resolver-spi-1.3.1.jar",
            "id": "org.apache.maven.resolver:maven-resolver-spi:null",
            "groupId": "org.apache.maven.resolver",
            "version": "1.3.1",
            "scope": "compile",
            "atrifact": "maven-resolver-spi-1.3.1.jar",
            "sha1": "7780f1f8ce061c3c8131d5cd91ad04e52c887091"
        },
        {
            "name": "commons-codec_commons-codec-1.11.jar",
            "location": "/commons-codec/commons-codec/1.11/commons-codec-1.11.jar",
            "id": "commons-codec:commons-codec:null",
            "groupId": "commons-codec",
            "version": "1.11",
            "scope": "compile",
            "atrifact": "commons-codec-1.11.jar",
            "sha1": "3acb4705652e16236558f0f4f2192cc33c3bd189"
        },
        {
            "name": "org.eclipse.sisu_org.eclipse.sisu.plexus-0.3.3.jar",
            "location": "/org/eclipse/sisu/org.eclipse.sisu.plexus/0.3.3/org.eclipse.sisu.plexus-0.3.3.jar",
            "id": "org.eclipse.sisu:org.eclipse.sisu.plexus:null",
            "groupId": "org.eclipse.sisu",
            "version": "0.3.3",
            "scope": "compile",
            "atrifact": "org.eclipse.sisu.plexus-0.3.3.jar",
            "sha1": "2c892c1fe0cd2dabcc729e1cbff3524b4847b1fe"
        },
        {
            "name": "org.slf4j_slf4j-api-1.7.25.jar",
            "location": "/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar",
            "id": "org.slf4j:slf4j-api:null",
            "groupId": "org.slf4j",
            "version": "1.7.25",
            "scope": "compile",
            "atrifact": "slf4j-api-1.7.25.jar",
            "sha1": "da76ca59f6a57ee3102f8f9bd9cee742973efa8a"
        },
        {
            "name": "commons-io_commons-io-2.6.jar",
            "location": "/commons-io/commons-io/2.6/commons-io-2.6.jar",
            "id": "commons-io:commons-io:null",
            "groupId": "commons-io",
            "version": "2.6",
            "scope": "compile",
            "atrifact": "commons-io-2.6.jar",
            "sha1": "815893df5f31da2ece4040fe0a12fd44b577afaf"
        },
        {
            "name": "org.apache.maven_maven-model-3.6.0.jar",
            "location": "/org/apache/maven/maven-model/3.6.0/maven-model-3.6.0.jar",
            "id": "org.apache.maven:maven-model:null",
            "groupId": "org.apache.maven",
            "version": "3.6.0",
            "scope": "compile",
            "atrifact": "maven-model-3.6.0.jar",
            "sha1": "06d73e6218a10cfe82cf0325b582cbed732cc751"
        },
        {
            "name": "org.eclipse.sisu_org.eclipse.sisu.inject-0.3.3.jar",
            "location": "/org/eclipse/sisu/org.eclipse.sisu.inject/0.3.3/org.eclipse.sisu.inject-0.3.3.jar",
            "id": "org.eclipse.sisu:org.eclipse.sisu.inject:null",
            "groupId": "org.eclipse.sisu",
            "version": "0.3.3",
            "scope": "compile",
            "atrifact": "org.eclipse.sisu.inject-0.3.3.jar",
            "sha1": "b163fc1e714db5f9b389ec11f11950b5913e454c"
        },
        {
            "name": "com.google.inject_guice-4.2.1-no_aop.jar",
            "location": "/com/google/inject/guice/4.2.1/guice-4.2.1-no_aop.jar",
            "id": "com.google.inject:guice:no_aop",
            "groupId": "com.google.inject",
            "version": "4.2.1",
            "scope": "compile",
            "atrifact": "guice-4.2.1-no_aop.jar",
            "sha1": "41e5ab52ec65e60b6c0ced947becf7ba96402645"
        },
        {
            "name": "org.apache.maven_maven-repository-metadata-3.6.0.jar",
            "location": "/org/apache/maven/maven-repository-metadata/3.6.0/maven-repository-metadata-3.6.0.jar",
            "id": "org.apache.maven:maven-repository-metadata:null",
            "groupId": "org.apache.maven",
            "version": "3.6.0",
            "scope": "compile",
            "atrifact": "maven-repository-metadata-3.6.0.jar",
            "sha1": "ab6576d2d7d02c20d8dadb2fdf8571991a47bd0a"
        },
        {
            "name": "org.sonatype.plexus_plexus-cipher-1.4.jar",
            "location": "/org/sonatype/plexus/plexus-cipher/1.4/plexus-cipher-1.4.jar",
            "id": "org.sonatype.plexus:plexus-cipher:null",
            "groupId": "org.sonatype.plexus",
            "version": "1.4",
            "scope": "compile",
            "atrifact": "plexus-cipher-1.4.jar",
            "sha1": "50ade46f23bb38cd984b4ec560c46223432aac38"
        },
        {
            "name": "javax.annotation_jsr250-api-1.0.jar",
            "location": "/javax/annotation/jsr250-api/1.0/jsr250-api-1.0.jar",
            "id": "javax.annotation:jsr250-api:null",
            "groupId": "javax.annotation",
            "version": "1.0",
            "scope": "compile",
            "atrifact": "jsr250-api-1.0.jar",
            "sha1": "5025422767732a1ab45d93abfea846513d742dcf"
        },
        {
            "name": "org.codehaus.plexus_plexus-classworlds-2.5.2.jar",
            "location": "/org/codehaus/plexus/plexus-classworlds/2.5.2/plexus-classworlds-2.5.2.jar",
            "id": "org.codehaus.plexus:plexus-classworlds:null",
            "groupId": "org.codehaus.plexus",
            "version": "2.5.2",
            "scope": "compile",
            "atrifact": "plexus-classworlds-2.5.2.jar",
            "sha1": "4abb111bfdace5b8167db4c0ef74644f3f88f142"
        },
        {
            "name": "javax.inject_javax.inject-1.jar",
            "location": "/javax/inject/javax.inject/1/javax.inject-1.jar",
            "id": "javax.inject:javax.inject:null",
            "groupId": "javax.inject",
            "version": "1",
            "scope": "compile",
            "atrifact": "javax.inject-1.jar",
            "sha1": "6975da39a7040257bd51d21a231b76c915872d38"
        },
        {
            "name": "org.apache.maven.resolver_maven-resolver-util-1.3.1.jar",
            "location": "/org/apache/maven/resolver/maven-resolver-util/1.3.1/maven-resolver-util-1.3.1.jar",
            "id": "org.apache.maven.resolver:maven-resolver-util:null",
            "groupId": "org.apache.maven.resolver",
            "version": "1.3.1",
            "scope": "compile",
            "atrifact": "maven-resolver-util-1.3.1.jar",
            "sha1": "caf7691a5a0373e948bbb00b23b52ef4cbceee71"
        },
        {
            "name": "org.sonatype.sisu_sisu-inject-bean-1.4.2.jar",
            "location": "/org/sonatype/sisu/sisu-inject-bean/1.4.2/sisu-inject-bean-1.4.2.jar",
            "id": "org.sonatype.sisu:sisu-inject-bean:null",
            "groupId": "org.sonatype.sisu",
            "version": "1.4.2",
            "scope": "compile",
            "atrifact": "sisu-inject-bean-1.4.2.jar",
            "sha1": "5cf37202afbaae899d63dd51b46d173df650af1b"
        },
        {
            "name": "org.apache.maven_maven-builder-support-3.6.0.jar",
            "location": "/org/apache/maven/maven-builder-support/3.6.0/maven-builder-support-3.6.0.jar",
            "id": "org.apache.maven:maven-builder-support:null",
            "groupId": "org.apache.maven",
            "version": "3.6.0",
            "scope": "compile",
            "atrifact": "maven-builder-support-3.6.0.jar",
            "sha1": "91247204af8e1b256f3d644f736a8acad76794e5"
        },
        {
            "name": "org.codehaus.plexus_plexus-component-annotations-1.7.1.jar",
            "location": "/org/codehaus/plexus/plexus-component-annotations/1.7.1/plexus-component-annotations-1.7.1.jar",
            "id": "org.codehaus.plexus:plexus-component-annotations:null",
            "groupId": "org.codehaus.plexus",
            "version": "1.7.1",
            "scope": "compile",
            "atrifact": "plexus-component-annotations-1.7.1.jar",
            "sha1": "862abca6deff0fff241a835a33d22559e9132069"
        },
        {
            "name": "org.apache.maven_maven-settings-builder-3.6.0.jar",
            "location": "/org/apache/maven/maven-settings-builder/3.6.0/maven-settings-builder-3.6.0.jar",
            "id": "org.apache.maven:maven-settings-builder:null",
            "groupId": "org.apache.maven",
            "version": "3.6.0",
            "scope": "compile",
            "atrifact": "maven-settings-builder-3.6.0.jar",
            "sha1": "5b889fc4eb0d322af9a4366c69507d1d5d51a2b8"
        },
        {
            "name": "com.google.errorprone_error_prone_annotations-2.2.0.jar",
            "location": "/com/google/errorprone/error_prone_annotations/2.2.0/error_prone_annotations-2.2.0.jar",
            "id": "com.google.errorprone:error_prone_annotations:null",
            "groupId": "com.google.errorprone",
            "version": "2.2.0",
            "scope": "compile",
            "atrifact": "error_prone_annotations-2.2.0.jar",
            "sha1": "88e3c593e9b3586e1c6177f89267da6fc6986f0c"
        },
        {
            "name": "org.sonatype.sisu_sisu-guice-2.1.7-noaop.jar",
            "location": "/org/sonatype/sisu/sisu-guice/2.1.7/sisu-guice-2.1.7-noaop.jar",
            "id": "org.sonatype.sisu:sisu-guice:noaop",
            "groupId": "org.sonatype.sisu",
            "version": "2.1.7",
            "scope": "compile",
            "atrifact": "sisu-guice-2.1.7-noaop.jar",
            "sha1": "8cb56e976b8e0e7b23f2969c32bef7b830c6d6ed"
        },
        {
            "name": "com.google.guava_failureaccess-1.0.1.jar",
            "location": "/com/google/guava/failureaccess/1.0.1/failureaccess-1.0.1.jar",
            "id": "com.google.guava:failureaccess:null",
            "groupId": "com.google.guava",
            "version": "1.0.1",
            "scope": "compile",
            "atrifact": "failureaccess-1.0.1.jar",
            "sha1": "1dcf1de382a0bf95a3d8b0849546c88bac1292c9"
        },
        {
            "name": "javax.enterprise_cdi-api-1.0.jar",
            "location": "/javax/enterprise/cdi-api/1.0/cdi-api-1.0.jar",
            "id": "javax.enterprise:cdi-api:null",
            "groupId": "javax.enterprise",
            "version": "1.0",
            "scope": "compile",
            "atrifact": "cdi-api-1.0.jar",
            "sha1": "44c453f60909dfc223552ace63e05c694215156b"
        },
        {
            "name": "org.codehaus.mojo_animal-sniffer-annotations-1.17.jar",
            "location": "/org/codehaus/mojo/animal-sniffer-annotations/1.17/animal-sniffer-annotations-1.17.jar",
            "id": "org.codehaus.mojo:animal-sniffer-annotations:null",
            "groupId": "org.codehaus.mojo",
            "version": "1.17",
            "scope": "compile",
            "atrifact": "animal-sniffer-annotations-1.17.jar",
            "sha1": "f97ce6decaea32b36101e37979f8b647f00681fb"
        },
        {
            "name": "org.apache.maven_maven-artifact-3.6.0.jar",
            "location": "/org/apache/maven/maven-artifact/3.6.0/maven-artifact-3.6.0.jar",
            "id": "org.apache.maven:maven-artifact:null",
            "groupId": "org.apache.maven",
            "version": "3.6.0",
            "scope": "compile",
            "atrifact": "maven-artifact-3.6.0.jar",
            "sha1": "d4c0da647de59c9ccc304a112fe1f1474d49e8eb"
        },
        {
            "name": "org.apache.maven_maven-settings-3.6.0.jar",
            "location": "/org/apache/maven/maven-settings/3.6.0/maven-settings-3.6.0.jar",
            "id": "org.apache.maven:maven-settings:null",
            "groupId": "org.apache.maven",
            "version": "3.6.0",
            "scope": "compile",
            "atrifact": "maven-settings-3.6.0.jar",
            "sha1": "c54a81d238ccdd58f7aeed785e29ac8f3a42fa21"
        },
        {
            "name": "org.apache.maven_maven-core-3.6.0.jar",
            "location": "/org/apache/maven/maven-core/3.6.0/maven-core-3.6.0.jar",
            "id": "org.apache.maven:maven-core:null",
            "groupId": "org.apache.maven",
            "version": "3.6.0",
            "scope": "compile",
            "atrifact": "maven-core-3.6.0.jar",
            "sha1": "f455f85cfa58962efd1dc63c7bcc0e034cd07594"
        },
        {
            "name": "com.google.j2objc_j2objc-annotations-1.1.jar",
            "location": "/com/google/j2objc/j2objc-annotations/1.1/j2objc-annotations-1.1.jar",
            "id": "com.google.j2objc:j2objc-annotations:null",
            "groupId": "com.google.j2objc",
            "version": "1.1",
            "scope": "compile",
            "atrifact": "j2objc-annotations-1.1.jar",
            "sha1": "ed28ded51a8b1c6b112568def5f4b455e6809019"
        },
        {
            "name": "com.google.guava_guava-27.0.1-jre.jar",
            "location": "/com/google/guava/guava/27.0.1-jre/guava-27.0.1-jre.jar",
            "id": "com.google.guava:guava:null",
            "groupId": "com.google.guava",
            "version": "27.0.1-jre",
            "scope": "compile",
            "atrifact": "guava-27.0.1-jre.jar",
            "sha1": "bd41a290787b5301e63929676d792c507bbc00ae"
        },
        {
            "name": "com.google.code.findbugs_jsr305-3.0.2.jar",
            "location": "/com/google/code/findbugs/jsr305/3.0.2/jsr305-3.0.2.jar",
            "id": "com.google.code.findbugs:jsr305:null",
            "groupId": "com.google.code.findbugs",
            "version": "3.0.2",
            "scope": "compile",
            "atrifact": "jsr305-3.0.2.jar",
            "sha1": "25ea2e8b0c338a877313bd4672d3fe056ea78f0d"
        },
        {
            "name": "com.google.guava_listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar",
            "location": "/com/google/guava/listenablefuture/9999.0-empty-to-avoid-conflict-with-guava/listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar",
            "id": "com.google.guava:listenablefuture:null",
            "groupId": "com.google.guava",
            "version": "9999.0-empty-to-avoid-conflict-with-guava",
            "scope": "compile",
            "atrifact": "listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar",
            "sha1": "b421526c5f297295adef1c886e5246c39d4ac629"
        },
        {
            "name": "org.apache.maven_maven-model-builder-3.6.0.jar",
            "location": "/org/apache/maven/maven-model-builder/3.6.0/maven-model-builder-3.6.0.jar",
            "id": "org.apache.maven:maven-model-builder:null",
            "groupId": "org.apache.maven",
            "version": "3.6.0",
            "scope": "compile",
            "atrifact": "maven-model-builder-3.6.0.jar",
            "sha1": "c9a8b1367163af0ed8887e8422f361b66e67a6d5"
        },
        {
            "name": "org.sonatype.sisu_sisu-inject-plexus-1.4.2.jar",
            "location": "/org/sonatype/sisu/sisu-inject-plexus/1.4.2/sisu-inject-plexus-1.4.2.jar",
            "id": "org.sonatype.sisu:sisu-inject-plexus:null",
            "groupId": "org.sonatype.sisu",
            "version": "1.4.2",
            "scope": "compile",
            "atrifact": "sisu-inject-plexus-1.4.2.jar",
            "sha1": "53d863ed4879d4a43ad7aee7bc63f935cc513353"
        },
        {
            "name": "org.apache.maven.shared_maven-shared-utils-3.2.1.jar",
            "location": "/org/apache/maven/shared/maven-shared-utils/3.2.1/maven-shared-utils-3.2.1.jar",
            "id": "org.apache.maven.shared:maven-shared-utils:null",
            "groupId": "org.apache.maven.shared",
            "version": "3.2.1",
            "scope": "compile",
            "atrifact": "maven-shared-utils-3.2.1.jar",
            "sha1": "08dd4dfb1d2d8b6969f6462790f82670bcd35ce2"
        },
        {
            "name": "org.apache.maven.shared_maven-common-artifact-filters-3.0.1.jar",
            "location": "/org/apache/maven/shared/maven-common-artifact-filters/3.0.1/maven-common-artifact-filters-3.0.1.jar",
            "id": "org.apache.maven.shared:maven-common-artifact-filters:null",
            "groupId": "org.apache.maven.shared",
            "version": "3.0.1",
            "scope": "compile",
            "atrifact": "maven-common-artifact-filters-3.0.1.jar",
            "sha1": "1a98d8e3d5610bb0abf09a7756195e8b2ed215e5"
        },
        {
            "name": "org.apache.maven.shared_maven-artifact-transfer-0.10.1.jar",
            "location": "/org/apache/maven/shared/maven-artifact-transfer/0.10.1/maven-artifact-transfer-0.10.1.jar",
            "id": "org.apache.maven.shared:maven-artifact-transfer:null",
            "groupId": "org.apache.maven.shared",
            "version": "0.10.1",
            "scope": "compile",
            "atrifact": "maven-artifact-transfer-0.10.1.jar",
            "sha1": "1b81cb430d23e1099db972c9b9dbfdae1b4374ff"
        },
        {
            "name": "org.codehaus.plexus_plexus-interpolation-1.25.jar",
            "location": "/org/codehaus/plexus/plexus-interpolation/1.25/plexus-interpolation-1.25.jar",
            "id": "org.codehaus.plexus:plexus-interpolation:null",
            "groupId": "org.codehaus.plexus",
            "version": "1.25",
            "scope": "compile",
            "atrifact": "plexus-interpolation-1.25.jar",
            "sha1": "3b37b3335e6a97e11e690bbdc22ade1a5deb74d6"
        },
        {
            "name": "com.jsoniter_jsoniter-0.9.23.jar",
            "location": "/com/jsoniter/jsoniter/0.9.23/jsoniter-0.9.23.jar",
            "id": "com.jsoniter:jsoniter:null",
            "groupId": "com.jsoniter",
            "version": "0.9.23",
            "scope": "compile",
            "atrifact": "jsoniter-0.9.23.jar",
            "sha1": "4cff9a02d6d9a848d1b7298aac36c47fd9e67d77"
        },
        {
            "name": "org.apache.maven_maven-plugin-api-3.6.0.jar",
            "location": "/org/apache/maven/maven-plugin-api/3.6.0/maven-plugin-api-3.6.0.jar",
            "id": "org.apache.maven:maven-plugin-api:null",
            "groupId": "org.apache.maven",
            "version": "3.6.0",
            "scope": "compile",
            "atrifact": "maven-plugin-api-3.6.0.jar",
            "sha1": "1ad37c33e3f046a84e7394df36a05a7f3b877d55"
        },
        {
            "name": "org.apache.commons_commons-lang3-3.8.1.jar",
            "location": "/org/apache/commons/commons-lang3/3.8.1/commons-lang3-3.8.1.jar",
            "id": "org.apache.commons:commons-lang3:null",
            "groupId": "org.apache.commons",
            "version": "3.8.1",
            "scope": "compile",
            "atrifact": "commons-lang3-3.8.1.jar",
            "sha1": "6505a72a097d9270f7a9e7bf42c4238283247755"
        },
        {
            "name": "org.codehaus.plexus_plexus-utils-3.1.1.jar",
            "location": "/org/codehaus/plexus/plexus-utils/3.1.1/plexus-utils-3.1.1.jar",
            "id": "org.codehaus.plexus:plexus-utils:null",
            "groupId": "org.codehaus.plexus",
            "version": "3.1.1",
            "scope": "compile",
            "atrifact": "plexus-utils-3.1.1.jar",
            "sha1": "b296e62bbdb9b4f018adffbd5e8e0aaa34b8c718"
        },
        {
            "name": "org.apache.maven.resolver_maven-resolver-api-1.3.1.jar",
            "location": "/org/apache/maven/resolver/maven-resolver-api/1.3.1/maven-resolver-api-1.3.1.jar",
            "id": "org.apache.maven.resolver:maven-resolver-api:null",
            "groupId": "org.apache.maven.resolver",
            "version": "1.3.1",
            "scope": "compile",
            "atrifact": "maven-resolver-api-1.3.1.jar",
            "sha1": "4a8cb0bf19651f6869353609065bb893bb033ef6"
        },
        {
            "name": "aopalliance_aopalliance-1.0.jar",
            "location": "/aopalliance/aopalliance/1.0/aopalliance-1.0.jar",
            "id": "aopalliance:aopalliance:null",
            "groupId": "aopalliance",
            "version": "1.0",
            "scope": "compile",
            "atrifact": "aopalliance-1.0.jar",
            "sha1": "0235ba8b489512805ac13a8f9ea77a1ca5ebe3e8"
        },
        {
            "name": "org.sonatype.plexus_plexus-sec-dispatcher-1.4.jar",
            "location": "/org/sonatype/plexus/plexus-sec-dispatcher/1.4/plexus-sec-dispatcher-1.4.jar",
            "id": "org.sonatype.plexus:plexus-sec-dispatcher:null",
            "groupId": "org.sonatype.plexus",
            "version": "1.4",
            "scope": "compile",
            "atrifact": "plexus-sec-dispatcher-1.4.jar",
            "sha1": "43fde524e9b94c883727a9fddb8669181b890ea7"
        },
        {
            "name": "org.checkerframework_checker-qual-2.5.2.jar",
            "location": "/org/checkerframework/checker-qual/2.5.2/checker-qual-2.5.2.jar",
            "id": "org.checkerframework:checker-qual:null",
            "groupId": "org.checkerframework",
            "version": "2.5.2",
            "scope": "compile",
            "atrifact": "checker-qual-2.5.2.jar",
            "sha1": "cea74543d5904a30861a61b4643a5f2bb372efc4"
        },
        {
            "name": "org.apache.maven_maven-resolver-provider-3.6.0.jar",
            "location": "/org/apache/maven/maven-resolver-provider/3.6.0/maven-resolver-provider-3.6.0.jar",
            "id": "org.apache.maven:maven-resolver-provider:null",
            "groupId": "org.apache.maven",
            "version": "3.6.0",
            "scope": "compile",
            "atrifact": "maven-resolver-provider-3.6.0.jar",
            "sha1": "feacf8ad983e794952562febef0a4962eec1c22e"
        },
        {
            "name": "org.apache.maven.resolver_maven-resolver-impl-1.3.1.jar",
            "location": "/org/apache/maven/resolver/maven-resolver-impl/1.3.1/maven-resolver-impl-1.3.1.jar",
            "id": "org.apache.maven.resolver:maven-resolver-impl:null",
            "groupId": "org.apache.maven.resolver",
            "version": "1.3.1",
            "scope": "compile",
            "atrifact": "maven-resolver-impl-1.3.1.jar",
            "sha1": "200e0386412e9317f9be5e3c00a18eda673e6f6d"
        }
    ]
}

```
